# Lernfeld 5 - Arrays

![](./images/Arrays.jpg)

## Eindimensionale Arrays

### Infoblatt: 
[IB eindimensionale Arrays](IB eindimensionale Arrays.md)

Folgende Aufgaben sind enthalten:
* [Random Numbers*](./src/randomNumbers/task.md)
* [Volume Filter*](./src/volumeFilter/task.md)
* [Array Calculator*-**](./src/arrayCalculator/task.md)
* [Temperature Analyzer**](./src/analyzingTemperatureData/task.md)

## Zweidimensionale Arrays

### Infoblätter: 
* [IB zweidimensionale Arrays](IB zweidimensionale Arrays.md)
* [IB Exceptions](IB Exceptions.md)

Folgende Aufgaben sind enthalten:
* [Array Sum 2d **](./src/arraySum2d/task.md)
* [Game of Life **](./src/gameOfLife/task.md)
* [Candy Crush ***](./src/candyCrush/task.md)
* [Image Processing ***](./src/imageProcessing/task.md)

